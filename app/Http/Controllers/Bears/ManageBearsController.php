<?php

namespace App\Http\Controllers\Bears;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Bears\Bear;

class ManageBearsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $data = [
            "bears" => Bear::get()
        ];


        return view("bears.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("bears.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        return Bear::create([
            "name"          => $input["name"],
            "danger_level"  => $input["danger_level"],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Bear $bear)
    {
        return $bear;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Bear $bear)
    {
        $data= [
            'bear'  => $bear
        ];
        return view("bears.edit",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bear $bear)
    {
        $input = $request->all();
        $bear->name = $input["name"];
        $bear->danger_level = $input["danger_level"];
        $bear->save();

        if ($bear->save()) {
            # code...
            return $bear;
        }

        abort(500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bear $deletable_bear)
    {

        $deletable_bear->delete();

        return redirect("bears");
    }
}

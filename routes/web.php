<?php

use App\Models\Bears\Bear;
use App\Models\Bears\Tree;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});



// pruebas de queries a la tabla de osos
// /**
//  * Seleccionar todos los osos
//  */
// Route::get('bears', function () {
//     return Bear::with("trees")->get();
// });

/**
 * Seleccionar todos los osos
 */
Route::get('trees', function () {
    return Tree::with(["bears" => function($q) {
                    return $q->bads();
                }])->get();
});

/**
 * Seleccionar todos los osos
 */
Route::get('trees/{type}', function ($type) {
    return Tree::where("type", "=", $type)->get();
});

Route::get('bears-by-tree-type/{type}', function ($type) {

    return Bear::likeTreesByType($type)->get();

    // return Tree::where("type", "=", $type)->get();
});

Route::get('bad-bears-by-tree-type/{type}', function ($type) {

    return Bear::likeTreesByType($type)->bads()->get();

    // return Tree::where("type", "=", $type)->get();
});


Route::get('bad-bears', function () {

    return Bear::bads()->get();


    return Bear::get()->filter(function($bear) {
                return $bear->is_bad;
            });
});


/**
 * Seleccionar todos los osos
 */
Route::get('bears-with-trees', function () {
    return Bear::has("trees")->get();
});


// /**
//  * insertar un nuevo registro de oso ro
//  */
// Route::get('bears/create', function () {
//
//     $new_bear = Bear::create([
//          'name'         => "ro",
//          'danger_level' => 5,
//     ]);
//
//     return $new_bear;
// });
//
// /**
//  * actualizar el nombre de un oso llamado ro
//  */
// Route::get('bears/{bear_id}', function ($bear_id) {
//     return Bear::whereHas("trees",function($q){
//         return $q->where("type" ,"=","robles");
//     })->with("trees")->find($bear_id);
// });

/**
 * selecionar los osos por grado de peligro
 */
Route::get('bears/danger_levels/{level}', function ($level) {
    return Bear::where("danger_level", "=", $level)->get();
});



Route::group(['namespace'=>'Bears'], function(){
Route::resource('bears', "ManageBearsController", [
    'only' => ['index', "show", "edit", "store", "update", "create"],
]);    
});



Route::delete('bears/{deletable_bear}', "Bears\ManageBearsController@destroy");

Route::bind("deletable_bear", function($deletable_bear) {
    return Bear::doesntHave("trees")->find($deletable_bear);
});

<?php

use Illuminate\Database\Seeder;
use App\Models\Bears\Bear;
use App\Models\Bears\Tree;

/**
 * 
 * @author Sergio Antonio Ochoa Martinez<gnu.java.sergio@gmail.com>
 */
class BearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trees = Tree::get();
        factory(Bear::class,120)->create()->each(function($bear) use ($trees){

            $trees_random = $trees->random( rand(10,20) );
            $bear->trees()->sync($trees_random);
        });
    }
}

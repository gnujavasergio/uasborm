<?php

use Illuminate\Database\Seeder;
use App\Models\Bears\Tree;

/**
 * 
 * @author Sergio Antonio Ochoa Martinez<gnu.java.sergio@gmail.com>
 */
class TreesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tree::class,1000)->create();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Bears\Forest;

/**
 * 
 * @author Sergio Antonio Ochoa Martinez<gnu.java.sergio@gmail.com>
 */
class ForestsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        factory(Forest::class, 5)->create();
    }
}
